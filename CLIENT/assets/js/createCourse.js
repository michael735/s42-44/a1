const createForm = document.getElementById(`createCourse`)
const token = localStorage.getItem('token')

createForm.addEventListener("submit", (e) =>{
    e.preventDefault()  

    let cn = document.getElementById(`courseName`).value
    let desc = document.getElementById(`description`).value
    let price = document.getElementById(`price`).value


    fetch(`http://localhost:4005/api/courses/create`,{
        method: "POST",
        headers:{
            "Content-Type" : "application/JSON",
            "Authorization" : `Bearer ${token}`
        },
        body: JSON.stringify({
            courseName: cn,
            description: desc,
            price: price
        })
    }).then(result => result.json())
    .then(result =>{
        if(result){
            alert('Course successfully added!')

            window.location.replace('./courses.html')
        } else {
            alert(`Please try again`)
        }
    })

})